;;; -*- coding: utf-8; Mode: Lisp -*-

;; Для пересоздания док-та выполни (ОЯ-ИНФРАСТРУКТУРА:Собрать-проект-Html-файла :ДОК-Ч115)

(in-package #:asdf)
(named-readtables::in-readtable :buddens-readtable-a)

(defsystem :ДОК-Ч115
 :serial t
 :depends-on (:СПРАВОЧНИКИ-HTML)
 :components
  ((:file "пакет-док-ч115")
   (:file "очистить-БД-и-заполнить-теги")
   (:file "док-ч115-бд")
   ))

