function АвтоТестДляПреобразуйСтрокуИзЛатиницы() {
  var УдачныхТестов = 0;
  function Сообщи(Сообщение) {
    console.log(Сообщение);
  }
  function Неудача(Сообщение) {
    console.error('НЕУДАЧА: '+Сообщение);
  }
  function ЛР(имяТеста,латиница, должноПолучиться) {
    Сообщи("ЛР "+имяТеста);
    var получилось = ПреобразуйСтрокуИзЛатиницы(латиница);
    if (!!!получилось) {
      Неудача('получилась пустота');
    }
    if (!(получилось == должноПолучиться)) {
      Неудача('получилось «'+получилось+'» вместо «'+должноПолучиться+'»');
    } else {
      УдачныхТестов++;
    }
  }
  Сообщи('АвтоТестДляПреобразуйСтрокуИзЛатиницы');
  var ОжидалосьУдачныхТестов = 11
  ЛР('ЧаЩа','SHH shh SH sh CH ch','Щ щ Ш ш Ч ч');
  ЛР('Неверное h 1','ph','^');
  ЛР('Неверные x 1','xe x',' ^');
  ЛР('Неверное j 1','j','^');
  ЛР('Неверное j 2','ojp','о^');
  ЛР('Неверные x 2','x ','^');
  ЛР('Цитрус','V chashhakh juga zhil by citrus? Da, no falqshivyjj ehkzempljar!','В чащах юга жил бы цитрус? Да, но фальшивый экземпляр!')
  ЛР('W','xewwWW','wW');
  ЛР('Неверное W','Wow','^о^');
  ЛР('Юникод',"xu2020xxu262dxxu2665xxu263cx",'†☭♥☼');
  ЛР('Неверный юникод',"xu1234567x xuZx",'^x ^Z^');

  if (УдачныхТестов != ОжидалосьУдачныхТестов) {
    console.error('АвтоТестДляПреобразуйСтрокуИзЛатиницы: прошло '
      +УдачныхТестов.toString()+' удачных тестов, а ожидалось '+ОжидалосьУдачныхТестов.toString());
  } else {
    Сообщи('АвтоТестДляПреобразуйСтрокуИзЛатиницы: УСПЕХ');
  }
}

АвтоТестДляПреобразуйСтрокуИзЛатиницы();

/* (C) Denis Budyak 2017-2019 
 
Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/ 
