module.exports = {
  /*
  ** Headers of the page
  */
  /*router: {
    extendRoutes (routes, resolve) {
      routes.push({
        //path: encodeURI('/яроклава-js.яргт'),
        //redirect: resolve(__dirname,'static/яроклава-js.яргт')
        path: '/bulk',
        redirect: '/'
      });
    }
  }, */
  head: {
    title: 'nuxt1',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
